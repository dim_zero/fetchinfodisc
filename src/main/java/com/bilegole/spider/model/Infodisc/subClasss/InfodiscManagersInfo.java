package com.bilegole.spider.model.Infodisc.subClasss;

public class InfodiscManagersInfo {
    String managerId;
    String managerName;
    String managerUrl;
    
    public String getManagerId() {
        return managerId;
    }

    public InfodiscManagersInfo setManagerId(String managerId) {
        this.managerId = managerId;
        return this;
    }

    public String getManagerName() {
        return managerName;
    }

    public InfodiscManagersInfo setManagerName(String managerName) {
        this.managerName = managerName;
        return this;
    }

    public String getManagerUrl() {
        return managerUrl;
    }

    public InfodiscManagersInfo setManagerUrl(String managerUrl) {
        this.managerUrl = managerUrl;
        return this;
    }

}
