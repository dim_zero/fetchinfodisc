package com.bilegole.spider.model.Infodisc;

import com.bilegole.spider.model.Infodisc.subClasss.InfodiscManagersInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InfodiscDoPofFund {
    Date establishDate;
    String fundName;
    String fundNo;
    String id;
    String isDeputeManage;
    Boolean lastQuarterUpdate;
    String managerName;
    String managerType;

    @JsonIgnore
    List<InfodiscManagersInfo> managersInfo;
    //    String managersInfo;
    String mandatorName;
    Long putOnRecordDate;
    String url;
    String workingState;

    public Date getEstablishDate() {
        return establishDate;
    }

    public InfodiscDoPofFund setEstablishDate(Date establishDate) {
        this.establishDate = establishDate;
        return this;
    }

    public String getFundName() {
        return fundName;
    }

    public InfodiscDoPofFund setFundName(String fundName) {
        this.fundName = fundName;
        return this;
    }

    public String getFundNo() {
        return fundNo;
    }

    public InfodiscDoPofFund setFundNo(String fundNo) {
        this.fundNo = fundNo;
        return this;
    }

    public String getId() {
        return id;
    }

    public InfodiscDoPofFund setId(String id) {
        this.id = id;
        return this;
    }

    public String getIsDeputeManage() {
        return isDeputeManage;
    }

    public InfodiscDoPofFund setIsDeputeManage(String isDeputeManage) {
        this.isDeputeManage = isDeputeManage;
        return this;
    }

    public Boolean getLastQuarterUpdate() {
        return lastQuarterUpdate;
    }

    public InfodiscDoPofFund setLastQuarterUpdate(Boolean lastQuarterUpdate) {
        this.lastQuarterUpdate = lastQuarterUpdate;
        return this;
    }

    public String getManagerName() {
        return managerName;
    }

    public InfodiscDoPofFund setManagerName(String managerName) {
        this.managerName = managerName;
        return this;
    }

    public String getManagerType() {
        return managerType;
    }

    public InfodiscDoPofFund setManagerType(String managerType) {
        this.managerType = managerType;
        return this;
    }

    public List<InfodiscManagersInfo> getManagersInfo() {
        return managersInfo;
    }

    public InfodiscDoPofFund setManagersInfo(List<InfodiscManagersInfo> managersInfo) {
        this.managersInfo = managersInfo;
        return this;
    }

    public String getMandatorName() {
        return mandatorName;
    }

    public InfodiscDoPofFund setMandatorName(String mandatorName) {
        this.mandatorName = mandatorName;
        return this;
    }

    public Long getPutOnRecordDate() {
        return putOnRecordDate;
    }

    public InfodiscDoPofFund setPutOnRecordDate(Long putOnRecordDate) {
        this.putOnRecordDate = putOnRecordDate;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public InfodiscDoPofFund setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getWorkingState() {
        return workingState;
    }

    public InfodiscDoPofFund setWorkingState(String workingState) {
        this.workingState = workingState;
        return this;
    }

}
