package com.bilegole.spider;

import com.bilegole.spider.model.Infodisc.InfodiscDoPofFund;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.ir.ObjectNode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RunTests {
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void contextLoads() {
    }

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void homeResponse() {
        String body = this.restTemplate.getForObject("/", String.class);
        assertThat(body).isEqualTo("Spring is here!");
    }

    @Test
    public void fetchPofFund_Object() throws IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rand", "0.22183779141074522");
        map.put("page", 0);
        map.put("size", 20);
        JsonNode result = restTemplate.postForObject(
                "https://gs.amac.org.cn/amac-infodisc/api/pof/fund?rand=0.22183779141074522&page=0&size=20",
                map,
                JsonNode.class
        ).at("/content");
        System.out.println(result);
        List<InfodiscDoPofFund> resSet = objectMapper.readValue(result.traverse(), new TypeReference<List<InfodiscDoPofFund>>() {
        });
        System.out.println(resSet);
    }

    @Test
    public void fetchPofFund_Entity() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("rand", "0.22183779141074522");
        map.put("page", 0);
        map.put("size", 20);
        Object result = restTemplate.postForEntity(
                "https://gs.amac.org.cn/amac-infodisc/api/pof/fund?rand=0.22183779141074522&page=0&size=20",
                map,
                InfodiscDoPofFund.class
        );
        System.out.println(result);
    }
}
